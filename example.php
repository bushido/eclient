<?php
/**
 * Created by PhpStorm.
 * User: desire
 * Date: 30.04.16
 * Time: 6:18
 */


require_once "vendor/autoload.php";



//Create new client. Register method should be used only on platforms with right access level
$client = new Evault\Client();

$client->setEmail("test@email.com");

$client->setAccountName("test");

$client->setPassword("test");

$status = $client->register();


if ($status != "success") {
    print("fail");
    exit();
}

// Create new app. It is possible to supply api_key to be used instead of one inside client
$app = new Evault\Application($client);

$app->setName("HELLO");

$status = $app->create();


//Creating vault
$vault = new Evault\Vault($client);

$vault->setName("NEW_VAULT");

$status = $vault->create();


// It is possible to create client with key
$client = new Evault\Client("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiNTcyZmE4ZDlhNTRkNzUxNDA2OWFmZDJhIiwiaXNzIjoiNTcyZmE4ZDlhNTRkNzUxNDA2OWFmZDJiIiwidHlwZSI6Im1hc3RlciJ9.yyYazqeopywKQZWuViCJ8XXASasie1YNdV4mBABj-YI");

$document = new Evault\Document($client, ".enc.key", "572fa984a54d75140f99a3a3");

$document->setDocument(json_encode(["wtf"=>"yes"]));

$status = $document->save();
