<?php
/**
 * Created by PhpStorm.
 * User: desire
 * Date: 30.04.16
 * Time: 6:21
 */

namespace Evault;

class Client
{   

    // this parameters are needed when creating account. Only usable by clients with proper
    // access rights
    public $email;

    public $password;

    public $accountName;

    public $apiKey;


    public $accountId;

    // instance of guzzle client
    public $transport;

    // Default endpoints
    // Those will change on production environment. Needs to be stable urls
    // Represent public addresses of datastore and keystore
    const keystore = "localhost:8000/v1/";

    const datastore = "localhost:8001/v1/";

    const success_res = "success";


    /**
     * Client constructor. Makes guzzle http client. Can use provided api_key (this is default behaviour in clients
     * with lower access rights.
     * @param null $api_key
     */
    public function __construct($api_key=null)
    {
        $this->transport = new \GuzzleHttp\Client();
        if (!is_null($api_key)) {
            $this->apiKey = $api_key;
        }

    }

    // Not all libraries should provide this method
    public function register() {

        $res = $this->transport->request('POST', Client::keystore."accounts/", [
            'form_params' => [
                'email' => $this->getEmail(),
                'password' => $this->getPassword(),
                'account_name' => $this->getAccountName()
            ]
        ]);


        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        // success is the OK response from the api 
        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setAccountId($response["account"]["account_id"]);
        $this->setApiKey($response["account"]["api_key"]);

        return $response["result"];
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param mixed $accountName
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }


    /**
     * @return mixed
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param mixed $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }


}
