<?php
/**
 * Created by PhpStorm.
 * User: desire
 * Date: 30.04.16
 * Time: 18:02
 */

namespace Evault;

use \ParagonIE\Halite\Symmetric\Crypto;
use \ParagonIE\Halite\KeyFactory;

class Document
{
    public $vault_id;

    public $requestKey;

    public $keyPath;

    public $document;

    public $document_id;

    private $client;


    private $enc_key;

    /**
     * Document constructor.
     * @param $client
     * @param $requestKey
     * @param $keyPath path to the encryption key made by Halite library (wrapper of libsodium). All encryption is
     * a duty of client.
     * @param $vault_id
     * @throws \ParagonIE\Halite\Alerts\CannotPerformOperation
     */
    public function __construct($client, $keyPath, $vault_id,$requestKey=null)
    {
        $this->client= $client;
        if (!is_null($requestKey)) {
            $this->setRequestKey($requestKey);
        } else {
            $this->setRequestKey($this->client->getApiKey());
        }
        $this->keyPath = $keyPath;
        $this->enc_key = KeyFactory::loadEncryptionKey($keyPath);
        $this->vault_id = $vault_id;

    }

    // Encrypt document using client's keys
    public function save() {

        // Encrypt docuemnt. Document can be any string. For future scheme search support document
        // can be restricted to be valid json document
        $ciphertext = Crypto::encrypt(
            $this->getDocument(),
            $this->enc_key
        );


        $res = $this->client->getTransport()->request('POST', Client::datastore."vaults/".$this->vault_id."/documents", [
            'form_params' => [
                'document' => base64_encode($ciphertext),
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);


        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setDocumentId($response["document_id"]);

        return $response["result"];
    }

    // Get data and decrypt using client's keys
    public function get($document_id) {
        $res = $this->client->getTransport()->request('GET', Client::datastore."vaults/".$this->vault_id."/documents/".$document_id, [
            'headers' => [
                'Content-type' => 'application/x-www-form-urlencoded'
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);

        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        // Decrypt document using key 
        $docInfo = Crypto::decrypt(
            base64_decode($response["document"]["document"]),
            $this->enc_key
        );

        $this->setDocument($docInfo);
        $this->setDocumentId($document_id);
        return $response["result"];

    }


    /**
     * @return mixed
     */
    public function getVaultId()
    {
        return $this->vault_id;
    }

    /**
     * @param mixed $vault_id
     */
    public function setVaultId($vault_id)
    {
        $this->vault_id = $vault_id;
    }

    /**
     * @return mixed
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * @param mixed $requestKey
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;
    }

    /**
     * @return mixed
     */
    public function getKeyPath()
    {
        return $this->keyPath;
    }

    /**
     * @param mixed $keyPath
     */
    public function setKeyPath($keyPath)
    {
        $this->keyPath = $keyPath;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getDocumentId()
    {
        return $this->document_id;
    }

    /**
     * @param mixed $document_id
     */
    public function setDocumentId($document_id)
    {
        $this->document_id = $document_id;
    }

}
