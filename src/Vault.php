<?php
/**
 * Created by PhpStorm.
 * User: desire
 * Date: 30.04.16
 * Time: 17:54
 */

namespace Evault;

class Vault
{

    public $name;

    public $requestKey;

    public $vault_id;

    private $client;


    /**
     * Vault constructor.
     * @param $client
     * @param $requestKey
     */
    public function __construct($client, $requestKey=null)
    {
        $this->client = $client;
        if (!is_null($requestKey)) {
            $this->setRequestKey($requestKey);
        } else {
            $this->setRequestKey($client->getApiKey());
        }
    }

    // Save vault to the account
    public function create() {

        $res = $this->client->getTransport()->request('POST', Client::datastore."vaults/", [
            'form_params' => [
                'name' => $this->getName()
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);


        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setVaultId($response["vault"]["vault_id"]);

        return $response["result"];
    }

    // Retrieve vault information by id
    public function get($vault_id) {
        $res = $this->client->getTransport()->request('GET', Client::datastore."vaults/".$vault_id, [
            'headers' => [
                'Content-type' => 'application/x-www-form-urlencoded'
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);

        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setName($response["vault"]["name"]);
        return $response["result"];
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * @param mixed $requestKey
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;
    }

    /**
     * @return mixed
     */
    public function getVaultId()
    {
        return $this->vault_id;
    }

    /**
     * @param mixed $vault_id
     */
    public function setVaultId($vault_id)
    {
        $this->vault_id = $vault_id;
    }





}
