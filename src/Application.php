<?php
/**
 * Created by PhpStorm.
 * User: desire
 * Date: 30.04.16
 * Time: 17:21
 */

namespace Evault;


class Application
{
    // for now errors thrown are Guzzle Http errors

    public $application_id;

    private $client;

    // required parameter
    public $name;

    public $users;

    public $apiKey;

    public $requestKey;

    /**
     * Application constructor. Sets client which contains guzzle http client and possibly api key.
     * If api key is not provided it is possible to use other api_key (request key parameter)
     * @param $client
     * @param $requestKey default null
     */
    public function __construct($client, $requestKey=null)
    {
        $this->client = $client;
        if (!is_null($requestKey)) {
            $this->requestKey = $requestKey;
        } else {
            $this->requestKey = $this->client->getApiKey();
        }


    }


    public function create() {

        // Makes guzzle http post request to the needed endpoint
        // Auth is made in basic auth header with login:pw where login is key
        // form_params sets header to application/x-www-form-urlencoded
        $res = $this->client->getTransport()->request('POST', Client::keystore."applications/", [
            'form_params' => [
                'name' => $this->getName(),
                'users' => $this->getUsers()
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);

        // parse response to php array
        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        // Better exceptions handling is coming
        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setApplicationId($response["application"]["application_id"]);
        $this->setApiKey($response["application"]["api_key"]);

        return $response["result"];
    }

    public function get($application_id) {

        // For now x-www-form-urlencoded header is a must.
        $res = $this->client->getTransport()->request('GET', Client::keystore."applications/".$application_id, [
            'headers' => [
                'Content-type' => 'application/x-www-form-urlencoded'
            ],
            'auth' => [
                $this->getRequestKey(), ""
            ]
        ]);


        $response = \GuzzleHttp\json_decode($res->getBody(), true);

        if ($response["result"] != Client::success_res) {
            return $response["result"];
        }

        $this->setApplicationId($application_id);
        $this->setName($response["application"]["name"]);
        $this->setUsers($response["application"]["users"]);
        return $response["result"];
    }

    // various default getters and setters


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }


    /**
     * @return mixed
     */
    public function getApplicationId()
    {
        return $this->application_id;
    }

    /**
     * @param mixed $application_id
     */
    public function setApplicationId($application_id)
    {
        $this->application_id = $application_id;
    }

    /**
     * @return mixed
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * @param mixed $requestKey
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;
    }

}
