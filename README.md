# Evault client library for php

Client library to access Evault's API.


## Dependencies 


There are two main dependencies:

* [GuzzleHttp](https://github.com/guzzle/guzzle) is used as HTTP client 

* [Halite](https://github.com/paragonie/halite) high-level cryptography library build upon Libsodium. For installation inconstructions refer to the official documentation. 


**Later Halite will be swapped for its underlying library Libsodium because of licensing issues**

## Installation 

For now this library is installed from private bitbucket repo. Access can be granted by repo's administrator.

Minimal composer.json to add library:

```json 
{
    "require": {
        "bushido/eclient": "dev-master",
    },
    "repositories": [
        {
            "type": "git",
            "url": "git@bitbucket.org:bushido/eclient.git"
        }
    ]
}

```

## Notes

Client should:

* Check for appropriate errors 

* Manage encryption key for different purposes



## Notes for the developers developing solutions in other languages

**Libraries in other languages should use Libsodium for their cryptography functionality**

Existing functionality implements following `API` calls to the `datastore` and `keystore`:

`KEYSTORE_HOST` and `DATASTORE_HOST` are urls of keystore and datastore APIs with appropriate versions. This library uses `http://localhost:8000/v1/` for the `Keystore` and `http://localhost:8001/v1/` for the `Datastore`. In current release library implements following API endpoints (for thorough information refer to the swagger docs in appropriate repositories):

* `POST` `KEYSTORE_HOST/accounts/` creates new account for API usage. Account is on the top of hierarchy. Meaning that this call should be made only by our own backends. 

* `POST` `KEYSTORE_HOST/applications/` creates application which groups users and groups of users

* `GET` `KEYSTORE_HOST/applications/{application_id}` retrieves information about specific application

* `POST` `DATASTORE_HOST/vaults/` creates vault

* `GET` `DATASTORE_HOST/vaults/{vault_id}` retrieves information about vault

* `POST` `DATASTORE_HOST/vaults/{vault_id}/documents/` adds document to the vault. *Note* encryption is performed by the client using libsodium(preferably)

* `GET` `DATASTORE_HOST/vaults/{vault_id}/documents/{document_id}` retrieves document from the datastore


## Proposed usage 

```php
//Create new client. Register method should be used only on platforms with right access level
$client = new Evault\Client();

$client->setEmail("test@email.com");

$client->setAccountName("test");

$client->setPassword("test");

$status = $client->register();


if ($status != "success") {
    print("fail");
    exit();
}

// Create new app. It is possible to supply api_key to be used instead of one inside client
$app = new Evault\Application($client);

$app->setName("HELLO");

$status = $app->create();


//Creating vault
$vault = new Evault\Vault($client);

$vault->setName("NEW_VAULT");

$status = $vault->create();


// It is possible to create client with key
$client = new Evault\Client("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiNTcyZmE4ZDlhNTRkNzUxNDA2OWFmZDJhIiwiaXNzIjoiNTcyZmE4ZDlhNTRkNzUxNDA2OWFmZDJiIiwidHlwZSI6Im1hc3RlciJ9.yyYazqeopywKQZWuViCJ8XXASasie1YNdV4mBABj-YI");

$document = new Evault\Document($client, ".enc.key", "572fa984a54d75140f99a3a3");

$document->setDocument(json_encode(["wtf"=>"yes"]));

$status = $document->save();


```


**Example on how to generate encryption key**

```php
//generate
$enc_key = \ParagonIE\Halite\KeyFactory::generateEncryptionKey();
//save to the desired location
\ParagonIE\Halite\KeyFactory::save($enc_key, '.enc.key');

```



## Documentation

API documentation will improve by describing more functionality.


## License

Lorem ipsum
