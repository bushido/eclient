<?php

require "vendor/autoload.php";

$enc_key = \ParagonIE\Halite\KeyFactory::generateEncryptionKey();

\ParagonIE\Halite\KeyFactory::save($enc_key, '.enc.key');
